#pragma once
class iSignWsHelper
	{
	public:
		iSignWsHelper();
		~iSignWsHelper();
		int readRegistry();

	private:
		std::string m_iSignWsInstallDir = "";
		std::string m_sllCertName = "";

	public:
		std::string getCertFileName();
		std::string getKeyFileName();

	};

