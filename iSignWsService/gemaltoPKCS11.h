#pragma once
#include "cryptoki.h"

class GemaltoPKCS11
	{
	public:
		GemaltoPKCS11();
		~GemaltoPKCS11();
		bool loadPKCS11();

		void unloadPKCS11();

		std::string getAllUserCertificates(const std::string & pin);

		std::string getCertificatesOfUser(const std::string & pin, const std::string & certificateSerialNumber);

		std::string sign(const std::string & pin, const std::string & base64data, const std::string & certificateSerialNumber, const std::string & algorithm);

		std::string SignCertificate(CK_SESSION_HANDLE hSession, const std::string & certificateSerialNumber, const std::string & base64data, const std::string & algorithm);

		std::list<std::string> getCaRootCertificates(const std::string & cnStr);

		void logout(CK_SESSION_HANDLE hSession);

		std::string login(const std::string & pin, CK_SESSION_HANDLE hSession);

		void getCertificates(CK_SESSION_HANDLE hSession, CK_ULONG certCategory);

		void loadCertificateInfo(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hCert, CK_ULONG certCategory);

		std::string stringToHex(const std::string & input);

		std::string stringToHex(const char * input, int len);

		std::string hexToString(const std::string & input);

		std::string getAttributeValue(CK_ULONG certCategory, const std::string & certificateSerialNumber, const std::string & key);

		std::string parseForName(std::string sText);

		std::string getLoginErrMessage(int rv);

		std::string getSignErrMessage(int retCode);

		void attributeErrMessage(int rv);

	private:
		HINSTANCE m_hPkcs11Lib = NULL;
		CK_FUNCTION_LIST_PTR m_pFunctionList = NULL;
		CK_C_GetFunctionList m_pGetFunctionList = NULL;
		CK_SLOT_ID m_slot;
		int m_rv;

		CK_SLOT_ID m_slots[20];
		CK_ULONG m_count = 20;

		std::string m_base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		std::string m_etokenReadError = "GoSignAir.CertPopUp.Message.Read.Failed";
		std::string m_msgOk = "OK";
		std::string m_certificateKey = "certificate_key";
		std::string m_subjectKey = "subject_key";
		std::string m_issuerKey = "issuer_key";
		std::string m_userCertKey = "user_cert_key";
		std::string m_caCertKey = "ca_cert_key";

		std::map< std::string, std::map<std::string, std::string> > m_userCerts, m_caCerts;
		std::string m_dbgstr;
	};

