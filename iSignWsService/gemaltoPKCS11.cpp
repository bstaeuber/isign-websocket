#include "windows.h"
#include <map>
#include <json/json.h> // uses JsonCppLib
#include "CicBase64.h"
#include <wincrypt.h>
#include <algorithm>
#include <list>
#include "gemaltoPKCS11.h"

#include "LoggerInclude.h"
#define cn "GemaltoPKCS11"

#define eToken_path         L"etoken.dll"
#define pkcs11_path         L"etpkcs11.dll"
#define DIM(a) (sizeof(a)/sizeof(a[0]))
#define MY_ENCODING_TYPE      X509_ASN_ENCODING | PKCS_7_ASN_ENCODING

GemaltoPKCS11::GemaltoPKCS11()
	{

	}


GemaltoPKCS11::~GemaltoPKCS11()
	{
	}

bool GemaltoPKCS11::loadPKCS11()
	{
	// Load the PKCS #11 library
	m_hPkcs11Lib = LoadLibrary(pkcs11_path);
	if (!m_hPkcs11Lib)
		{
		LOGR_ERR(cn, "LoadPKCS11: Cannot load DLL");
		return false;
		}

	// Find the entry point of C_GetFunctionList
	m_pGetFunctionList = (CK_C_GetFunctionList)GetProcAddress(m_hPkcs11Lib, "C_GetFunctionList");
	if (!m_pGetFunctionList)
		{
		LOGR_ERR(cn, "LoadPKCS11: Cannot find GetFunctionList");
		return false;
		}

	//Get the function list
	m_rv = m_pGetFunctionList(&m_pFunctionList);
	if (m_rv)
		{
		LOGR_ERR(cn, "LoadPKCS11: Can't get function list");
		return false;
		}

	return true;
	}

void GemaltoPKCS11::unloadPKCS11()
	{
	LOGR(cn, "UnloadPKCS11: UnloadPKCS11");
	m_pFunctionList = NULL;

	if (m_hPkcs11Lib)
		{
		FreeLibrary(m_hPkcs11Lib);
		}
	}

//get user certificates, returned map of user certs
//map: key: certificate serial number
//	   value: subject -> CN value
//parameter: pin value
//return json string
std::string GemaltoPKCS11::getAllUserCertificates(const std::string& pin)
	{
	std::string json_string;
	std::map<std::string, std::string> jsonMap;

	//CK_OBJECT_HANDLE  hObject;
	CK_SESSION_HANDLE hSession;

	// Categorization of the certificate: 
	// 0 = unspecified(defaultvalue), 1 = token user, 2 = authority, 3 = other entity
	CK_ULONG userCertCategory = 0; // get user Certificates

	LOGR(cn, "GetAllUserCertificates: Entering ...");

	//load PKCS 11
	if (!loadPKCS11())
		return m_etokenReadError;

	// Initialize the PKCS #11 library
	m_rv = m_pFunctionList->C_Initialize(NULL);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetAllUserCertificates: C_Initialize_failed");
		return m_etokenReadError;
		}

	// Get first occuped slot
	m_rv = m_pFunctionList->C_GetSlotList(TRUE, m_slots, &m_count);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetAllUserCertificates: C_GetSlotList_failed");
		return m_etokenReadError;
		}

	if (m_count < 1)
		{
		LOGR_ERR(cn, "GetAllUserCertificates: No_eToken_is_available");
		return m_etokenReadError;
		}

	m_slot = m_slots[0];

	LOGR(cn, "GetAllUserCertificates: Cryptoki library successful initialized");

	// Open sessionpkcs11_path
	m_rv = m_pFunctionList->C_OpenSession(m_slot, (CKF_SERIAL_SESSION | CKF_RW_SESSION), 0, 0, &hSession);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetAllUserCertificates: C_OpenSession_failed");
		return m_etokenReadError;
		}

	//Login
	std::string msg = login(pin, hSession);
	if (msg != m_msgOk)
		{
		LOGR_ERR(cn, "GetAllUserCertificates: Login_failed");
		return msg;
		}

	//Load user certificates
	getCertificates(hSession, userCertCategory);

	//get light map and convert to json base on userCerts
	for (auto const &ent1 : m_userCerts) {
		// ent1.first is the serial number key

		//for (auto const &ent2 : ent1.second) {
		//	ent2.first is the key (certificate, subject, issuer)
		//	ent2.second is the data
		//}
		std::string subject = getAttributeValue(userCertCategory, ent1.first, m_subjectKey);
		std::string cnStr = parseForName(subject);
		jsonMap[ent1.first] = cnStr;
		}

	//log out
	logout(hSession);

	//close resources
	unloadPKCS11();

	// Create a Json::Value object which will store the contents of the VariantMap
	Json::Value json_value;
	for (const auto& item : jsonMap)
		{
		json_value[item.first] = item.second;
		}

	// Create a StyledWriter which will convert our Json::Value to  a styled JSON string
	Json::StyledWriter writer;

	// Use the writer to write out the string
	json_string = writer.write(json_value);

	m_dbgstr = "GetAllUserCertificates: json_data - " + json_string;
	LOGR(cn, (char *)m_dbgstr.c_str());

	return json_string;
	}


//get user cert and ca certs of specific user certificate's CN in base64 encoded string
//return json string
std::string GemaltoPKCS11::getCertificatesOfUser(const std::string& pin, const std::string& certificateSerialNumber)
	{
	std::string json_string;
	std::map<std::string, std::list<std::string>> certs;

	m_dbgstr = "getCertificatesOfUser: " + certificateSerialNumber;
	LOGR(cn, (char *)m_dbgstr.c_str());

	//CK_OBJECT_HANDLE  hObject;
	CK_SESSION_HANDLE hSession;

	// Categorization of the certificate: 
	// 0 = unspecified(defaultvalue), 1 = token user, 2 = authority, 3 = other entity
	// had user certificates already 
	CK_ULONG caCertCategory = 2; // get user Certificates

	//load PKCS 11
	if (!loadPKCS11())
		return m_etokenReadError;

	// Initialize the PKCS #11 library
	m_rv = m_pFunctionList->C_Initialize(NULL);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetCertificatesOfUser - C_Initialize_failed");
		return m_etokenReadError;
		}

	// Get first occuped slot
	m_rv = m_pFunctionList->C_GetSlotList(TRUE, m_slots, &m_count);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetCertificatesOfUser - C_GetSlotList_failed");
		return m_etokenReadError;
		}

	if (m_count < 1)
		{
		LOGR_ERR(cn, "GetCertificatesOfUser - No_eToken_is_available");
		return m_etokenReadError;
		}

	m_slot = m_slots[0];

	LOGR(cn, "GetCertificatesOfUser - Cryptoki library successful initialized");

	// Open sessionpkcs11_path
	m_rv = m_pFunctionList->C_OpenSession(m_slot, (CKF_SERIAL_SESSION | CKF_RW_SESSION), 0, 0, &hSession);
	if (m_rv)
		{
		LOGR_ERR(cn, "GetCertificatesOfUser - C_OpenSession_failed");
		return m_etokenReadError;
		}

	//Login
	std::string msg = login(pin, hSession);
	if (msg != m_msgOk)
		{
		LOGR_ERR(cn, "GetCertificatesOfUser - Login_failed");
		return msg;
		}

	// had user certificates already, dont load again.
	// Load user certificates
	if (m_userCerts.empty())
		{
		LOGR(cn, "GetCertificatesOfUser - Load user certificates again if it is NULL!");
		getCertificates(hSession, 0);
		}

	// Load ca certificates
	getCertificates(hSession, caCertCategory);

	// fetch ca, root certificates for specific user certificate
	// load user cert from userCerts map by using serial number
	// put into map["user_cert_key", "user cert base 64 string" ]
	std::string userCertStr = getAttributeValue(0, certificateSerialNumber, m_certificateKey);
	std::list<std::string> userCertList;
	userCertList.push_back(userCertStr);
	certs[m_userCertKey] = userCertList;

	// load ca, root certs from caCerts map by using CN string value.
	// put into map["ca cert", list["ca certs base 64 string"] ]
	std::string issuer, issuer_cn;
	issuer = getAttributeValue(0, certificateSerialNumber, m_issuerKey);

	issuer_cn = parseForName(issuer);
	certs[m_caCertKey] = getCaRootCertificates(issuer_cn);

	//log out
	logout(hSession);

	//close resources
	unloadPKCS11();

	LOGR(cn, "GetCertificatesOfUser - got certificate, now creating json");

	// convert map to json_string

	// Create a Json::Value object which will store the contents of the VariantMap
	//Json::Value json_value = FB::variantToJsonValue(certs);

	Json::Value json_value;
	Json::Value json_array;
	for (const auto& item : certs)
		{
		if (item.first == m_userCertKey)
			{
			// has just 1 element
			std::list<std::string> tmpL = item.second;
			std::list<std::string>::iterator it = tmpL.begin();

			json_value[m_userCertKey] = *it;
			}
		if (item.first == m_caCertKey)
			{
			std::list<std::string> tmpL = item.second;
			for (const auto& lItem : tmpL)
				{
				json_array.append(lItem);
				}
			json_value[m_caCertKey] = json_array;
			}
		}

	// Create a StyledWriter which will convert our Json::Value to  a styled JSON string
	Json::StyledWriter writer;

	// Use the writer to write out the string
	json_string = writer.write(json_value);

	m_dbgstr = "GetCertificatesOfUser - json_data: " + json_string;
	LOGR_ERR(cn, (char *)m_dbgstr.c_str());

	return json_string;
	}


//sign, using SerialNumber to find certificate return signed certificate
//return json string
std::string GemaltoPKCS11::sign(const std::string& pin, const std::string& base64data, const std::string& certificateSerialNumber, const std::string& algorithm)
	{
	CK_SESSION_HANDLE hSession;
	std::string strcert;

	//load PKCS 11
	if (!loadPKCS11())
		return strcert;

	// Initialize the PKCS #11 library
	m_rv = m_pFunctionList->C_Initialize(NULL);
	if (m_rv)
		{
		LOGR_ERR(cn, "Sign - C_Initialize failed");
		return m_etokenReadError;
		}

	// Get first occuped slot
	m_rv = m_pFunctionList->C_GetSlotList(TRUE, m_slots, &m_count);
	if (m_rv)
		{
		LOGR_ERR(cn, "Sign - C_GetSlotList failed");
		return m_etokenReadError;
		}

	if (m_count < 1)
		{
		LOGR_ERR(cn, "Sign - No eToken is available");
		return m_etokenReadError;
		}

	m_slot = m_slots[0];

	LOGR(cn, "Sign - Cryptoki library successful initialized");

	// Open sessionpkcs11_path
	m_rv = m_pFunctionList->C_OpenSession(m_slot, (CKF_SERIAL_SESSION | CKF_RW_SESSION), 0, 0, &hSession);
	if (m_rv)
		{
		LOGR_ERR(cn, "Sign - C_OpenSession failed");
		return m_etokenReadError;
		}

	//Login
	std::string msg = login(pin, hSession);
	if (msg != m_msgOk)
		{
		LOGR_ERR(cn, "Sign - Login_failed");
		return msg;
		}

	strcert = SignCertificate(hSession, certificateSerialNumber, base64data, algorithm);

	m_dbgstr = "SignCertificate returning: " + strcert;
	LOGR_ERR(cn, (char *)m_dbgstr.c_str());

	//log out
	logout(hSession);

	//close resources
	unloadPKCS11();

	return strcert;
	}

std::string GemaltoPKCS11::SignCertificate(CK_SESSION_HANDLE hSession, const std::string& certificateSerialNumber, const std::string& base64data, const std::string& algorithm)
	{
	int	isPaddingRequired = 0;
	int isDataFromUser = 0;

	//CK_ATTRIBUTE      Template;
	CK_RV             retCode;
	CK_ULONG          usCount;
	CK_ULONG          usTotal;

	CK_MECHANISM mech;
	CK_OBJECT_HANDLE hObject;
	//CK_OBJECT_HANDLE hKey;

	CK_ULONG certCategory = 0; // user certificates
	CK_OBJECT_CLASS certObject = CKO_CERTIFICATE;
	CK_CERTIFICATE_TYPE x509Cert = CKC_X_509;
	CK_BBOOL _true = CK_TRUE;
	CK_BBOOL _false = CK_FALSE;

	char inputBuffer[500];
	CK_BYTE pSigData[3000];

	std::string signedData = "";
	CK_ULONG id = 0;

	LOGR(cn, "SignCertificate - SignCertificate enter");

#ifndef PKCS11_V1
	CK_ULONG usSigLen = sizeof(pSigData);
#else
	CK_ULONG usSigLen = 0;
#endif

	char *pInputData = 0;
	unsigned long ulInputDataLen = 0;
	CK_BYTE_PTR pInData = (CK_BYTE_PTR)inputBuffer;

	// using SHA1-RSA as default now";
	mech.mechanism = CKM_SHA1_RSA_PKCS;

	if (algorithm == "SHA256")
		{
		mech.mechanism = CKM_SHA256_RSA_PKCS; // SHA256 test
		LOGR(cn, "SignCertificate - is SHA256");
		}
	else
		LOGR(cn, "SignCertificate - is SHA1");


	mech.pParameter = 0;
	//mech.usParameterLen = 0;
	isDataFromUser = 0;

	CK_ATTRIBUTE pCertificateTemplate[] = {
			{ CKA_CLASS, 0, sizeof(certObject) },
			{ CKA_CERTIFICATE_TYPE, 0, sizeof(x509Cert) },
			{ CKA_TOKEN, &_true, sizeof(_true) },
			{ CKA_CERTIFICATE_CATEGORY, &certCategory, sizeof(certCategory) }
		},
		*pTemplate = 0;

	CK_ULONG usTemplateLen = 0;
	pCertificateTemplate[0].pValue = &certObject;
	pCertificateTemplate[1].pValue = &x509Cert;

	pTemplate = pCertificateTemplate;
	usTemplateLen = DIM(pCertificateTemplate);

	CK_ATTRIBUTE attrs[2];
	CK_OBJECT_CLASS cls = CKO_PRIVATE_KEY;
	CK_OBJECT_HANDLE signaturekey = CK_INVALID_HANDLE;
	CK_ULONG count;

	attrs[0].type = CKA_CLASS;
	attrs[0].pValue = &cls;
	attrs[0].ulValueLen = sizeof(cls);
	attrs[1].type = CKA_ID;

	//--------- get the hObject base on certificate serial number
	retCode = m_pFunctionList->C_FindObjectsInit(hSession, pTemplate, usTemplateLen);
	usCount = 1;
	usTotal = 0;

	//loop to find certs
	while (usCount != 0 && retCode == CKR_OK)
		{
		retCode = m_pFunctionList->C_FindObjects(hSession, &hObject, 1, &usCount);
		usTotal += usCount;
		if (usTotal == 0)
			{
			LOGR(cn, "SignCertificate - No certificates found");
			}
		else if (usCount != 0)
			{
			LOGR(cn, "SignCertificate - Certificates found");
			CK_ATTRIBUTE attributeValue, attributeId;
			CK_RV        retCode;

			// Get attribute size
			attributeValue.type = CKA_VALUE;
			attributeId.type = CKA_ID;

			attributeValue.pValue = NULL;
			attributeId.pValue = NULL;

			retCode = m_pFunctionList->C_GetAttributeValue(hSession, hObject, &attributeValue, 1);

			if (retCode == CKR_OK)
				{
				LOGR(cn, "SignCertificate - after C_GetAttributeValue");
				attributeValue.pValue = malloc(attributeValue.ulValueLen + 1);
				retCode = m_pFunctionList->C_GetAttributeValue(hSession, hObject, &attributeValue, 1);
				if (retCode == CKR_OK)
					{
					LOGR(cn, "SignCertificate - after C_GetAttributeValue 2");
					PCCERT_CONTEXT           pContext = NULL;
					pContext = CertCreateCertificateContext(MY_ENCODING_TYPE, (const BYTE *)attributeValue.pValue, attributeValue.ulValueLen);
					if (pContext)
						{
						LOGR(cn, "SignCertificate - after CCertCreateCertificateContext");
						PCERT_INFO pCertInfo = pContext->pCertInfo;
						unsigned char* pbData = pCertInfo->SerialNumber.pbData;
						int cbData = pCertInfo->SerialNumber.cbData;


						// check serial number, if equal then find ID
						std::string serial((char*)pbData);
						std::string serialSubstring = serial.substr(0, cbData);
						std::reverse(serialSubstring.begin(), serialSubstring.end());
						std::string snInHex(stringToHex(serialSubstring).c_str());
						m_dbgstr = "SignCertificate - certificate serial number" + snInHex;
						LOGR(cn, (char *)m_dbgstr.c_str());
						m_dbgstr = "SignCertificate - certificateSerialNumber will find" + certificateSerialNumber;
						LOGR(cn, (char *)m_dbgstr.c_str());

						// certificate is found -> get ID to use for signing
						if (certificateSerialNumber == snInHex)
							{
							LOGR(cn, "SignCertificate - Found certificate!");
							retCode = m_pFunctionList->C_GetAttributeValue(hSession, hObject, &attributeId, 1);

							LOGR(cn, "SignCertificate - Get attribute CKA_ID.");
							attributeErrMessage(retCode);

							if (retCode == CKR_OK)
								{
								attributeId.pValue = malloc(attributeId.ulValueLen + 1);
								retCode = m_pFunctionList->C_GetAttributeValue(hSession, hObject, &attributeId, 1);
								if (retCode == CKR_OK)
									{
									LOGR(cn, "SignCertificate - Update CKA_ID value for primary key.");
									// set CKA_ID
									attrs[1].pValue = attributeId.pValue;
									attrs[1].ulValueLen = attributeId.ulValueLen;
									}
								}

							break; // exit loop

							}//if (certificateSerialNumber == snInHex)

						}
					} //if (retCode == CKR_OK)
				else
					{
					LOGR_ERR(cn, "SignCertificate() - Failure to find certificate.");
					}

				free(attributeValue.pValue);
				free(attributeId.pValue);

				}// if (retCode == CKR_OK)
			else
				{
				LOGR_ERR(cn, "SignCertificate() - FC_GetAttributeValue failed.");
				}

			}
		} // while loop

	m_pFunctionList->C_FindObjectsFinal(hSession);

	// find hkey
	//Find the signature private key
	retCode = m_pFunctionList->C_FindObjectsInit(hSession, attrs, 2);

	if (retCode != CKR_OK)
		{
		LOGR_ERR(cn, "SignCertificate - C_FindObjectsInit failed.");
		return m_etokenReadError;
		}

	retCode = m_pFunctionList->C_FindObjects(hSession, &signaturekey, 1, &count);

	if (retCode != CKR_OK)
		{
		LOGR_ERR(cn, "SignCertificate - C_FindObjects failed.");
		return m_etokenReadError;
		}

	if (count == 0)
		{
		LOGR_ERR(cn, "SignCertificate - Signature key not found.");
		return m_etokenReadError;
		}

	retCode = m_pFunctionList->C_FindObjectsFinal(hSession);

	if (retCode != CKR_OK)
		{
		LOGR_ERR(cn, "SignCertificate - C_FindObjectsFinal failed.");
		return m_etokenReadError;
		}

	char *decodeBuffer = NULL;
	char *encodeBuffer = NULL;

	m_dbgstr = "SignCertificate - Bese64 digest len: " + base64data;
	LOGR(cn, (char *)m_dbgstr.c_str());

	CicBase64	base64;
	base64.CreateMatchingDecodingBuffer((char*)base64data.c_str(), (char**)&decodeBuffer);
	int decodeLen = base64.DecodeBuffer((char*)base64data.c_str(), (char*)decodeBuffer);

	m_dbgstr = "CIC Decoded digest length: "  + decodeLen;
	LOGR(cn, (char *)m_dbgstr.c_str());

	// not working correct 
	//std::string decodeData = base64_decode(base64data);
	//int decodeLen = decodeData.length();
	//wsprintfA(dbgstr, "Decoded digest len: %d", decodeLen);
	//FBLOG_INFO("SignCertificate", dbgstr);
	//char *data = new char[decodeData.length() + 1];
	//strcpy(data, decodeData.c_str());
	//memcpy(data, decodeData.data(), decodeLen);

	pInData = (CK_BYTE_PTR)decodeBuffer;
	ulInputDataLen = decodeLen;

	//FBLOG_DEBUG("SignCertificate", base64data);
	//FBLOG_DEBUG("SignCertificate", signaturekey);

	// Start signing
	LOGR(cn, "SignCertificate - C_SignInit");
	retCode = m_pFunctionList->C_SignInit(hSession, &mech, signaturekey);

	std::string msg = getSignErrMessage(retCode);
	if (msg != m_msgOk)
		{
		LOGR_ERR(cn, "SignCertificate - C_SignInit_failed");
		return msg;
		}

	CK_ULONG usInLen = (CK_ULONG)ulInputDataLen;

	// get the signature length
	if (retCode == CKR_OK)
		{
		LOGR(cn, "SignCertificate - C_Sign fetch len");
		retCode = m_pFunctionList->C_Sign(hSession, pInData, usInLen, (CK_BYTE_PTR)NULL_PTR, &usSigLen);

		msg = getSignErrMessage(retCode);
		if (msg != m_msgOk)
			{
			LOGR_ERR(cn, "SignCertificate - C_Sign_failed");
			return msg;
			}
		}

	// get the signature
	if (retCode == CKR_OK)
		{
		LOGR(cn, "SignCertificate - C_Sign");
		retCode = m_pFunctionList->C_Sign(hSession, pInData, usInLen, (CK_BYTE_PTR)pSigData, &usSigLen);

		msg = getSignErrMessage(retCode);
		if (msg != m_msgOk)
			{
			LOGR_ERR(cn, "SignCertificate - C_Sign_failed");
			return msg;
			}
		}

	// Write result to file
	if ((retCode == CKR_OK) && usSigLen)
		{
		LOGR(cn, "SignCertificate - encode digest");
		m_dbgstr = "Signature len: " + usSigLen;
		LOGR(cn, (char *)m_dbgstr.c_str());

		int encodeBufferLength = base64.CreateMatchingEncodingBuffer(usSigLen, (char**)&encodeBuffer);
		base64.EncodeBuffer((char*)pSigData, usSigLen, (char*)encodeBuffer);
		signedData.append(encodeBuffer, strlen(encodeBuffer));
		}

	if (decodeBuffer)
		free(decodeBuffer);
	if (encodeBuffer)
		free(encodeBuffer);

	m_dbgstr = "SignCertificate - signedData: " + signedData;
	LOGR(cn, (char *)m_dbgstr.c_str());

	return signedData;
	}

// return list of ca, root certificates
std::list<std::string> GemaltoPKCS11::getCaRootCertificates(const std::string& cnStr)
	{
	//FB::VariantList list;
	std::list<std::string> list;

	std::string search_cn = cnStr;
	std::string subject, subject_cn, issuer, issuer_cn;
	bool exit = false;

	m_dbgstr = "GetCaRootCertificates - search_cn" + search_cn;
	LOGR(cn, (char *)m_dbgstr.c_str());

	m_dbgstr = "GetCaRootCertificates - number ca certs: " + std::to_string(m_caCerts.size());
	LOGR(cn, (char *)m_dbgstr.c_str());

	for (auto const &ent1 : m_caCerts) {
		// ent1.first is the serial number key
		//FBLOG_DEBUG("GetCaRootCertificates", "ca cert serial number");
		//FBLOG_DEBUG("GetCaRootCertificates", ent1.first);

		//for (auto const &ent2 : ent1.second) {
		// ent2.first is the key (certificate, subject, issuer)
		// ent2.second is the data
		//}
		subject = getAttributeValue(2, ent1.first, m_subjectKey);
		issuer = getAttributeValue(2, ent1.first, m_issuerKey);

		subject_cn = parseForName(subject);
		issuer_cn = parseForName(issuer);

		m_dbgstr = "GetCaRootCertificates - subject_cn: " + subject_cn + " issuer_cn: " + search_cn;
		LOGR(cn, (char *)m_dbgstr.c_str());

		if (subject_cn.compare(search_cn) == 0)
			{
			m_dbgstr = "GetCaRootCertificates - subject_cn: " + subject_cn;
			LOGR(cn, (char *)m_dbgstr.c_str());

			m_dbgstr = "GetCaRootCertificates - issuer_cn" + issuer_cn;
			LOGR(cn, (char *)m_dbgstr.c_str());

			std::string certStr = getAttributeValue(2, ent1.first, m_certificateKey);

			LOGR(cn, "GetCaRootCertificates - Add found certificate");
			list.push_back(certStr);

			// not root certificate, continue to search from beginning
			if (subject_cn != issuer_cn)
				{
				LOGR(cn, "GetCaRootCertificates - reset-search");
				search_cn = issuer_cn;
				std::list<std::string> listTmp = getCaRootCertificates(search_cn); //Recursive

																					// move and empty listTmp
				list.splice(list.end(), listTmp);

				// copy
				//list.insert(list.end(), listTmp.begin(), listTmp.end());
				}
			else // root
				{
				// exit;
				LOGR(cn, "GetCaRootCertificates - Found root certificate, exit search");
				break;
				}

			}
		else
			{
			// cont. to search
			LOGR(cn, "issuer not equal subject, continue searching");
			}

		}// for loop

	return list;
	}

void GemaltoPKCS11::logout(CK_SESSION_HANDLE hSession)
	{
	LOGR(cn, "Logout: C_Logout");
	m_pFunctionList->C_Logout(hSession);

	LOGR(cn, "Logout: C_CloseSession");
	m_pFunctionList->C_CloseSession(hSession);

	LOGR(cn, "Logout: C_Finalize");
	m_pFunctionList->C_Finalize(0);
	}

std::string GemaltoPKCS11::login(const std::string& pin, CK_SESSION_HANDLE hSession)
	{
	std::string loginMsg;
	char *data = new char[pin.length() + 1];
	strcpy_s(data, pin.length() + 1, pin.c_str());

	m_rv = m_pFunctionList->C_Login(hSession, CKU_USER, (CK_BYTE_PTR)data, pin.length());
	loginMsg = getLoginErrMessage(m_rv);

	return loginMsg;
	}

void GemaltoPKCS11::getCertificates(CK_SESSION_HANDLE hSession, CK_ULONG certCategory)
	{
	CK_OBJECT_HANDLE  hObject;
	//CK_ATTRIBUTE      Template;
	CK_RV             retCode;
	CK_ULONG          usCount;
	CK_ULONG          usTotal;

	CK_OBJECT_CLASS certObject = CKO_CERTIFICATE;
	CK_CERTIFICATE_TYPE x509Cert = CKC_X_509;
	CK_BBOOL _true = CK_TRUE;

	LOGR(cn, "Fetching CA certs");

	CK_ATTRIBUTE pCertificateTemplate[] = {
			{ CKA_CLASS, 0, sizeof(certObject) },
			{ CKA_CERTIFICATE_TYPE, 0, sizeof(x509Cert) },
			{ CKA_CERTIFICATE_CATEGORY, &certCategory, sizeof(certCategory) }
		},
		*pTemplate = 0;

	CK_ULONG usTemplateLen = 0;
	pCertificateTemplate[0].pValue = &certObject;
	pCertificateTemplate[1].pValue = &x509Cert;

	// load user certificates
	pTemplate = pCertificateTemplate;
	usTemplateLen = DIM(pCertificateTemplate);
	m_dbgstr = "GetUserCertificates: certCategory - " + certCategory;
	LOGR(cn, (char *)m_dbgstr.c_str());

	retCode = m_pFunctionList->C_FindObjectsInit(hSession, pTemplate, usTemplateLen);
	usCount = 1;
	usTotal = 0;

	while (usCount != 0 && retCode == CKR_OK)
		{
		retCode = m_pFunctionList->C_FindObjects(hSession, &hObject, 1, &usCount);

		usTotal += usCount;
		if (usTotal == 0)
			{
			LOGR(cn, "GetUserCertificates: No certificates found");
			}
		else if (usCount != 0)
			{
			LOGR(cn, "GetUserCertificates: Certificates found");
			loadCertificateInfo(hSession, hObject, certCategory);
			}
		} // while loop

	m_dbgstr = "GetUserCertificates: usTotal certs - %d" + usTotal;
	LOGR(cn, (char *)m_dbgstr.c_str());

	m_pFunctionList->C_FindObjectsFinal(hSession);
	}

// Fill neccessary data to user certificate map
// map[serialnumber, map["certificate_key",certstringbase64; "subject_key", cn value; "issuer_key", issuer]]
void GemaltoPKCS11::loadCertificateInfo(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hCert, CK_ULONG certCategory)
	{
	std::map<std::string, std::string>		certInfo;
	CK_ATTRIBUTE	attribute;
	CK_RV			retCode;
	std::string		nameString;

	attribute.type = CKA_VALUE;
	attribute.pValue = NULL;
	char *encodeBuffer = NULL;

	CicBase64 base64;

	LOGR(cn, "loadCertificateInfo: enter");

	//return values from C_GetAttributeValue()
	//CKR_ARGUMENTS_BAD, CKR_ATTRIBUTE_SENSITIVE, CKR_ATTRIBUTE_TYPE_INVALID, CKR_BUFFER_TOO_SMALL, 
	//CKR_CRYPTOKI_NOT_INITIALIZED, CKR_DEVICE_ERROR, CKR_DEVICE_MEMORY, CKR_DEVICE_REMOVED, CKR_FUNCTION_FAILED, 
	//CKR_GENERAL_ERROR, CKR_HOST_MEMORY, CKR_OBJECT_HANDLE_INVALID, CKR_OK, CKR_SESSION_CLOSED, CKR_SESSION_HANDLE_INVALID.
	retCode = m_pFunctionList->C_GetAttributeValue(hSession, hCert, &attribute, 1);

	if (retCode == CKR_OK)
		{
		LOGR(cn, "loadCertificateInfo: C_GetAttributeValue OK");
		attribute.pValue = malloc(attribute.ulValueLen + 1);
		retCode = m_pFunctionList->C_GetAttributeValue(hSession, hCert, &attribute, 1);
		if (retCode == CKR_OK)
			{
			m_dbgstr = "loadCertificateInfo: C_GetAttributeValue 2 OK, length: " + std::to_string(attribute.ulValueLen);
			LOGR(cn, (char *)m_dbgstr.c_str());
			//certificate object
			std::string base64Str = "";

			int encodeBufferLength = base64.CreateMatchingEncodingBuffer(attribute.ulValueLen, (char**)&encodeBuffer);

			m_dbgstr = "loadCertificateInfo: CreateMatchingEncodingBuffer length: " + std::to_string(encodeBufferLength);
			LOGR(cn, (char *)m_dbgstr.c_str());

			base64.EncodeBuffer((char*)attribute.pValue, attribute.ulValueLen, (char*)encodeBuffer);
			LOGR(cn, "After EncodeBuffer");
			base64Str.append(encodeBuffer, strlen(encodeBuffer));
			LOGR(cn, "After append");
			free(encodeBuffer);
			m_dbgstr = "loadCertificateInfo: after free,  base64 length: " + std::to_string(base64Str.size());
			LOGR(cn, (char *)m_dbgstr.c_str());

			//m_dbgstr = "LoadCertificateInfo: cert base64Str - " + base64Str;
			//LOGR(cn, (char *)m_dbgstr.c_str());

			certInfo[m_certificateKey] = base64Str;

			PCCERT_CONTEXT           pContext = NULL;
			pContext = CertCreateCertificateContext(MY_ENCODING_TYPE, (const BYTE *)attribute.pValue, attribute.ulValueLen);

			if (pContext)
				{
				PCERT_INFO pCertInfo = pContext->pCertInfo;

				//Subject ( include CN (Issued to) )
				int size = CertNameToStrA(MY_ENCODING_TYPE, &pCertInfo->Subject, CERT_X500_NAME_STR, NULL, 0);
				if (size)
					{
					char *pText = new char[size];
					CertNameToStrA(MY_ENCODING_TYPE, &pCertInfo->Subject, CERT_X500_NAME_STR, pText, size);
					nameString = pText;

					certInfo[m_subjectKey] = nameString;

					m_dbgstr = "LoadCertificateInfo - subject key: " + nameString;
					LOGR(cn, (char *)m_dbgstr.c_str());
					delete[] pText;
					}

				//Issuer (Issued by)
				size = CertNameToStrA(MY_ENCODING_TYPE, &pCertInfo->Issuer, CERT_X500_NAME_STR, NULL, 0);
				if (size)
					{
					char *pText = new char[size];
					CertNameToStrA(MY_ENCODING_TYPE, &pCertInfo->Issuer, CERT_X500_NAME_STR, pText, size);
					nameString = pText;

					//save full
					certInfo[m_issuerKey] = nameString;

					m_dbgstr = "LoadCertificateInfo - cert issuer: " + nameString;
					LOGR(cn, (char *)m_dbgstr.c_str());
					delete[] pText;
					}

				// load certificate serial number
				// serial number is reversed in struct _CERT_INFO
				unsigned char* pbData = pCertInfo->SerialNumber.pbData;
				int cbData = pCertInfo->SerialNumber.cbData;

				std::string serial((char*)pbData);
				std::string serialSubstring = serial.substr(0, cbData);

				std::reverse(serialSubstring.begin(), serialSubstring.end());
				std::string snInHex(stringToHex(serialSubstring).c_str());

				m_dbgstr = "LoadCertificateInfo - snInHex: "+ snInHex;
				LOGR(cn, (char *)m_dbgstr.c_str());

				// put everything into map
				if (certCategory == 2)
					m_caCerts[snInHex] = certInfo;
				else
					m_userCerts[snInHex] = certInfo;

				}
			} //if (retCode == CKR_OK)
		else
			{
			LOGR(cn, "loadCertificateInfo: C_GetAttributeValue 2 FAILED");
			}

		free(attribute.pValue);

		LOGR(cn, "loadCertificateInfo: exit");

		}// if (retCode == CKR_OK)
	else
		LOGR(cn, "loadCertificateInfo: C_GetAttributeValue FAILED");

	}

std::string GemaltoPKCS11::stringToHex(const std::string& input)
	{
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();

	std::string output;
	output.reserve(2 * len);
	for (size_t i = 0; i < len; ++i)
		{
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 15]);
		}
	return output;
	}

std::string GemaltoPKCS11::stringToHex(const char* input, int len)
	{
	static const char* const lut = "0123456789ABCDEF";
	//size_t len = input.length();

	std::string output;
	output.reserve(2 * len);
	for (int i = 0; i < len; ++i)
		{
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 15]);
		}
	return output;
	}

std::string GemaltoPKCS11::hexToString(const std::string& input)
	{
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();
	if (len & 1) 
		throw std::invalid_argument("odd length");

	std::string output;
	output.reserve(len / 2);
	for (size_t i = 0; i < len; i += 2)
		{
		char a = input[i];
		const char* p = std::lower_bound(lut, lut + 16, a);
		if (*p != a) throw std::invalid_argument("not a hex digit");

		char b = input[i + 1];
		const char* q = std::lower_bound(lut, lut + 16, b);
		if (*q != b) throw std::invalid_argument("not a hex digit");

		output.push_back(((p - lut) << 4) | (q - lut));
		}
	return output;
	}

// get value of attribute from cert list: cer value, cn or issuer
// check return value is not empty
std::string GemaltoPKCS11::getAttributeValue(CK_ULONG certCategory, const std::string& certificateSerialNumber, const std::string& key)
	{
	std::map<std::string, std::map<std::string, std::string>>::iterator pos;
	std::string value;

	m_dbgstr = "GetAttributeValue - certCategory: " + certCategory;
	LOGR(cn, (char *)m_dbgstr.c_str());

	m_dbgstr = "GetAttributeValue - key: " + key;
	LOGR(cn, (char *)m_dbgstr.c_str());

	m_dbgstr = "GetAttributeValue - certificateSerialNumber: " + certificateSerialNumber;
	LOGR(cn, (char *)m_dbgstr.c_str());

	if (certCategory == 2)
		{
		pos = m_caCerts.find(certificateSerialNumber);
		if (pos == m_caCerts.end()) {
			//handle the error
			return "";
			}
		}
	else
		{
		pos = m_userCerts.find(certificateSerialNumber);
		if (pos == m_userCerts.end()) {
			//handle the error
			return "";
			}
		}

	std::map<std::string, std::string> subMap = pos->second;
	std::map<std::string, std::string>::iterator subpos = subMap.find(key);
	if (subpos == subMap.end()) {
		value = "";
		}
	else {
		value = subpos->second;
		//m_dbgstr = "GetAttributeValue - attribute value: " + value;
		//LOGR(cn, (char *)m_dbgstr.c_str());
		}

	return value;
	}

std::string GemaltoPKCS11::parseForName(std::string sText)
	{
	int ind1;
	int ind2;

	std::string tok1 = "CN=";
	std::string tok2 = ",";
	std::string tok3 = "O=";

	std::string str;

	//--------------------------------------------------------------------
	//	 now parsing
	//--------------------------------------------------------------------

	ind1 = sText.find(tok1, 0);

	if (ind1 == std::string::npos)
		{
		ind1 = sText.find(tok3, 0);
		}

	if (ind1 != std::string::npos)
		{
		ind1 += 3;
		ind2 = sText.find(tok2, ind1);

		if (ind2 != -1)
			{
			str = sText.substr(ind1, ind2 - ind1);
			}
		else
			{
			str = sText.substr(ind1);
			}
		}

	return str;
	}

std::string GemaltoPKCS11::getLoginErrMessage(int rv)
	{
	if (rv == CKR_OK) {
		return m_msgOk;
		}

	if (rv == CKR_ARGUMENTS_BAD)
		{
		LOGR(cn, "Login: CKR_ARGUMENTS_BAD");
		return "GoSignAir.CertPopUp.Message.CKR_ARGUMENTS_BAD";
		}

	if (rv == CKR_CRYPTOKI_NOT_INITIALIZED)
		{
		LOGR(cn, "Login: CKR_CRYPTOKI_NOT_INITIALIZED");
		return "GoSignAir.CertPopUp.Message.CKR_CRYPTOKI_NOT_INITIALIZED";

		}

	if (rv == CKR_DEVICE_ERROR)
		{
		LOGR(cn, "Login: CKR_DEVICE_ERROR");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_ERROR";
		}

	if (rv == CKR_DEVICE_MEMORY)
		{
		LOGR(cn, "Login: CKR_DEVICE_MEMORY");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_MEMORY";
		}

	if (rv == CKR_DEVICE_REMOVED)
		{
		LOGR(cn, "Login: CKR_DEVICE_REMOVED");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_REMOVED";
		}

	if (rv == CKR_FUNCTION_CANCELED)
		{
		LOGR(cn, "Login: CKR_FUNCTION_CANCELED");
		return "GoSignAir.CertPopUp.Message.CKR_FUNCTION_CANCELED";
		}

	if (rv == CKR_FUNCTION_FAILED)
		{
		LOGR(cn, "Login: CKR_FUNCTION_FAILED");
		return "GoSignAir.CertPopUp.Message.CKR_FUNCTION_FAILED";
		}

	if (rv == CKR_GENERAL_ERROR)
		{
		LOGR(cn, "Login: CKR_GENERAL_ERROR");
		return "GoSignAir.CertPopUp.Message.CKR_GENERAL_ERROR";
		}

	if (rv == CKR_HOST_MEMORY)
		{
		LOGR(cn, "Login: CKR_HOST_MEMORY");
		return "GoSignAir.CertPopUp.Message.CKR_HOST_MEMORY";
		}

	if (rv == CKR_OPERATION_NOT_INITIALIZED)
		{
		LOGR(cn, "Login: CKR_OPERATION_NOT_INITIALIZED");
		return "GoSignAir.CertPopUp.Message.CKR_OPERATION_NOT_INITIALIZED";
		}

	if (rv == CKR_PIN_INCORRECT)
		{
		LOGR(cn, "Login: CKR_PIN_INCORRECT");
		return "GoSignAir.CertPopUp.Message.CKR_PIN_INCORRECT";
		}

	if (rv == CKR_PIN_LOCKED)
		{
		LOGR(cn, "Login: CKR_PIN_LOCKED");
		return "GoSignAir.CertPopUp.Message.CKR_PIN_LOCKED";
		}

	if (rv == CKR_SESSION_CLOSED)
		{
		LOGR(cn, "Login: CKR_SESSION_CLOSED");
		return "GoSignAir.CertPopUp.Message.CKR_SESSION_CLOSED";
		}

	if (rv == CKR_SESSION_HANDLE_INVALID)
		{
		LOGR(cn, "Login: CKR_SESSION_HANDLE_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_SESSION_HANDLE_INVALID";
		}

	if (rv == CKR_SESSION_READ_ONLY_EXISTS)
		{
		LOGR(cn, "Login: CKR_SESSION_READ_ONLY_EXISTS");
		return "GoSignAir.CertPopUp.Message.CKR_SESSION_READ_ONLY_EXISTS";
		}

	if (rv == CKR_USER_ALREADY_LOGGED_IN)
		{
		LOGR(cn, "Login: CKR_USER_ALREADY_LOGGED_IN");

		LOGR(cn, "Login: OK, KEEP MOVING");

		//return "GoSignAir.CertPopUp.Message.CKR_USER_ALREADY_LOGGED_IN";
		return m_msgOk;
		}

	if (rv == CKR_USER_ANOTHER_ALREADY_LOGGED_IN)
		{
		LOGR(cn, "Login: CKR_USER_ANOTHER_ALREADY_LOGGED_IN");
		return "GoSignAir.CertPopUp.Message.CKR_USER_ANOTHER_ALREADY_LOGGED_IN";
		}

	if (rv == CKR_USER_PIN_NOT_INITIALIZED)
		{
		LOGR(cn, "Login: CKR_USER_PIN_NOT_INITIALIZED");
		return "GoSignAir.CertPopUp.Message.CKR_USER_PIN_NOT_INITIALIZED";
		}

	if (rv == CKR_USER_TOO_MANY_TYPES)
		{
		LOGR(cn, "Login: CKR_USER_TOO_MANY_TYPES");
		return "GoSignAir.CertPopUp.Message.CKR_USER_TOO_MANY_TYPES";
		}

	if (rv == CKR_USER_TYPE_INVALID)
		{
		LOGR(cn, "Login: CKR_USER_TYPE_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_USER_TYPE_INVALID";
		}

	return "";
	}

std::string GemaltoPKCS11::getSignErrMessage(int retCode)
	{
	m_dbgstr = "SignCertificate: SignMessage Code - " + retCode;
	OutputDebugStringA(m_dbgstr.c_str());

	if (retCode == CKR_ARGUMENTS_BAD) {
		LOGR(cn, "SignCertificate: CKR_ARGUMENTS_BAD");
		return "GoSignAir.CertPopUp.Message.CKR_ARGUMENTS_BAD";
		}

	if (retCode == CKR_CRYPTOKI_NOT_INITIALIZED) {
		LOGR(cn, "SignCertificate: CKR_CRYPTOKI_NOT_INITIALIZED");
		return "GoSignAir.CertPopUp.Message.CKR_CRYPTOKI_NOT_INITIALIZED";
		}

	if (retCode == CKR_DEVICE_ERROR) {
		LOGR(cn, "SignCertificate: CKR_DEVICE_ERROR");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_ERROR";
		}

	if (retCode == CKR_DEVICE_MEMORY) {
		LOGR(cn, "SignCertificate: CKR_DEVICE_MEMORY");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_MEMORY";
		}

	if (retCode == CKR_DEVICE_REMOVED) {
		LOGR(cn, "SignCertificate: CKR_DEVICE_REMOVED");
		return "GoSignAir.CertPopUp.Message.CKR_DEVICE_REMOVED";
		}

	if (retCode == CKR_FUNCTION_CANCELED) {
		LOGR(cn, "SignCertificate: CKR_FUNCTION_CANCELED");
		return "GoSignAir.CertPopUp.Message.CKR_FUNCTION_CANCELED";
		}

	if (retCode == CKR_FUNCTION_FAILED) {
		LOGR(cn, "SignCertificate: CKR_FUNCTION_FAILED");
		return "GoSignAir.CertPopUp.Message.CKR_FUNCTION_FAILED";
		}

	if (retCode == CKR_GENERAL_ERROR) {
		LOGR(cn, "SignCertificate: CKR_GENERAL_ERROR");
		return "GoSignAir.CertPopUp.Message.CKR_GENERAL_ERROR";
		}

	if (retCode == CKR_HOST_MEMORY) {
		LOGR(cn, "SignCertificate: CKR_HOST_MEMORY");
		return "GoSignAir.CertPopUp.Message.CKR_HOST_MEMORY";
		}

	if (retCode == CKR_KEY_FUNCTION_NOT_PERMITTED) {
		LOGR(cn, "SignCertificate: CKR_KEY_FUNCTION_NOT_PERMITTED");
		return "GoSignAir.CertPopUp.Message.CKR_KEY_FUNCTION_NOT_PERMITTED";
		}

	if (retCode == CKR_KEY_HANDLE_INVALID) {
		LOGR(cn, "SignCertificate: CKR_KEY_HANDLE_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_KEY_HANDLE_INVALID";
		}


	if (retCode == CKR_KEY_SIZE_RANGE) {
		LOGR(cn, "SignCertificate: CKR_KEY_SIZE_RANGE");
		return "GoSignAir.CertPopUp.Message.CKR_KEY_SIZE_RANGE";
		}

	if (retCode == CKR_KEY_TYPE_INCONSISTENT) {
		LOGR(cn, "SignCertificate: CKR_KEY_TYPE_INCONSISTENT");
		return "GoSignAir.CertPopUp.Message.CKR_KEY_TYPE_INCONSISTENT";
		}


	if (retCode == CKR_MECHANISM_INVALID) {
		LOGR(cn, "SignCertificate: CKR_MECHANISM_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_MECHANISM_INVALID";
		}


	if (retCode == CKR_MECHANISM_PARAM_INVALID) {
		LOGR(cn, "SignCertificate: CKR_MECHANISM_PARAM_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_MECHANISM_PARAM_INVALID";
		}


	if (retCode == CKR_OK) {
		LOGR(cn, "SignCertificate: CKR_OK");
		return m_msgOk;
		}


	if (retCode == CKR_OPERATION_ACTIVE) {
		LOGR(cn, "SignCertificate: CKR_OPERATION_ACTIVE");
		return "GoSignAir.CertPopUp.Message.CKR_OPERATION_ACTIVE";
		}


	if (retCode == CKR_PIN_EXPIRED) {
		LOGR(cn, "SignCertificate: CKR_PIN_EXPIRED");
		return "GoSignAir.CertPopUp.Message.CKR_PIN_EXPIRED";
		}


	if (retCode == CKR_SESSION_CLOSED) {
		LOGR(cn, "SignCertificate: CKR_SESSION_CLOSED");
		return "GoSignAir.CertPopUp.Message.CKR_SESSION_CLOSED";
		}


	if (retCode == CKR_SESSION_HANDLE_INVALID) {
		LOGR(cn, "SignCertificate: CKR_SESSION_HANDLE_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_SESSION_HANDLE_INVALID";
		}


	if (retCode == CKR_USER_NOT_LOGGED_IN) {
		LOGR(cn, "SignCertificate: CKR_USER_NOT_LOGGED_IN");
		return "GoSignAir.CertPopUp.Message.CKR_USER_NOT_LOGGED_IN";
		}

	if (retCode == CKR_BUFFER_TOO_SMALL) {
		LOGR(cn, "SignCertificate: CKR_BUFFER_TOO_SMALL");
		return "GoSignAir.CertPopUp.Message.CKR_BUFFER_TOO_SMALL";
		}

	if (retCode == CKR_DATA_INVALID) {
		LOGR(cn, "SignCertificate: CKR_DATA_INVALID");
		return "GoSignAir.CertPopUp.Message.CKR_DATA_INVALID";
		}

	if (retCode == CKR_DATA_LEN_RANGE) {
		LOGR(cn, "SignCertificate: CKR_DATA_LEN_RANGE");
		return "GoSignAir.CertPopUp.Message.CKR_DATA_LEN_RANGE";
		}

	return "";
	}

void GemaltoPKCS11::attributeErrMessage(int rv)
	{
	if (rv == CKR_ARGUMENTS_BAD)
		{
		LOGR(cn, "AttributeMessage: CKR_ARGUMENTS_BAD");
		}

	if (rv == CKR_ATTRIBUTE_SENSITIVE)
		{
		LOGR(cn, "AttributeMessage: CKR_ATTRIBUTE_SENSITIVE");
		}

	if (rv == CKR_ATTRIBUTE_TYPE_INVALID)
		{
		LOGR(cn, "AttributeMessage: CKR_ATTRIBUTE_TYPE_INVALID");
		}

	if (rv == CKR_BUFFER_TOO_SMALL)
		{
		LOGR(cn, "AttributeMessage: CKR_BUFFER_TOO_SMALL");
		}

	if (rv == CKR_CRYPTOKI_NOT_INITIALIZED)
		{
		LOGR(cn, "AttributeMessage: CKR_CRYPTOKI_NOT_INITIALIZED");
		}

	if (rv == CKR_DEVICE_ERROR)
		{
		LOGR(cn, "AttributeMessage: CKR_DEVICE_ERROR");
		}

	if (rv == CKR_DEVICE_MEMORY)
		{
		LOGR(cn, "AttributeMessage: CKR_DEVICE_MEMORY");
		}

	if (rv == CKR_DEVICE_REMOVED)
		{
		LOGR(cn, "AttributeMessage: CKR_DEVICE_REMOVED");
		}

	if (rv == CKR_FUNCTION_FAILED)
		{
		LOGR(cn, "AttributeMessage: CKR_FUNCTION_FAILED");
		}

	if (rv == CKR_GENERAL_ERROR)
		{
		LOGR(cn, "AttributeMessage: CKR_GENERAL_ERROR");
		}

	if (rv == CKR_HOST_MEMORY)
		{
		LOGR(cn, "AttributeMessage: CKR_HOST_MEMORY");
		}

	if (rv == CKR_OBJECT_HANDLE_INVALID)
		{
		LOGR(cn, "AttributeMessage: CKR_OBJECT_HANDLE_INVALID");
		}

	if (rv == CKR_OK)
		{
		LOGR(cn, "AttributeMessage: CKR_OK");
		}

	if (rv == CKR_SESSION_CLOSED)
		{
		LOGR(cn, "AttributeMessage: CKR_SESSION_CLOSED");
		}

	if (rv == CKR_SESSION_HANDLE_INVALID)
		{
		LOGR(cn, "AttributeMessage: CKR_SESSION_HANDLE_INVALID");
		}

	}