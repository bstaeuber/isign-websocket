#include "windows.h"
#include <string>

using namespace std;

#include "iSignWsHelper.h"
#include "LoggerInclude.h"

#define cn L"iSignWsHelper"

iSignWsHelper::iSignWsHelper()
	{
	readRegistry();
	}


iSignWsHelper::~iSignWsHelper()
	{
	}


int iSignWsHelper::readRegistry()
	{
	HKEY	hKey;
	DWORD	Type = REG_SZ;
	DWORD	BytesBack = 256;
	int		value = 0;
	char	buf[256];

	/*
	*-------------------------------------------------------------------------
	* Read back the values from the registry
	*-------------------------------------------------------------------------
	*/
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("Software\\CIC\\PenPal"), 0, KEY_READ, &hKey) == ERROR_SUCCESS)
		{
		if (RegQueryValueExA(hKey, "iSignWsInstallDir", 0, &Type, (LPBYTE)&buf, &BytesBack) == ERROR_SUCCESS)
			m_iSignWsInstallDir.append(buf);
		if (RegQueryValueExA(hKey, "iSignWsSSLCertBaseFileName", 0, &Type, (LPBYTE)&buf, &BytesBack) == ERROR_SUCCESS)
			m_sllCertName.append(buf);

		RegCloseKey(hKey);
		}

	return true;
	}


std::string iSignWsHelper::getCertFileName()
	{
	if (m_sllCertName.length() == 0)
		return std::string(m_iSignWsInstallDir + "\\iSignWsCert.crt");
	else 
		return std::string(m_iSignWsInstallDir + "\\" + m_sllCertName + ".crt");
	}


std::string iSignWsHelper::getKeyFileName()
	{
	if (m_sllCertName.length() == 0)
		return std::string(m_iSignWsInstallDir + "\\iSignWsCert.key");
	else
		return std::string(m_iSignWsInstallDir + "\\" + m_sllCertName + ".key");
	}
