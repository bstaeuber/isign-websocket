#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include "server_ws.hpp"
#include <json/json.h> // uses JsonCppLib
#include "GemaltoPKCS11.h"
#include "WebSocketServerWs.h"
#include "LoggerInclude.h"

#define cn "WebSocketServerWs"

using namespace std;
using WsServer = SimpleWeb::SocketServer<SimpleWeb::WS>;

WebSocketServerWs::WebSocketServerWs()
	{
	}


WebSocketServerWs::~WebSocketServerWs()
	{
	}


int WebSocketServerWs::runServer()
	{
	WsServer *wsServer = NULL;
	std::string dbgstr;

	LOGR(cn, "WebSocketServerWs::runServer called.");

	try
		{
		wsServer = new WsServer();
		wsServer->config.port = 9022;
		}
	catch (std::exception const& e)
		{
		dbgstr = "Instanciating WsServer exception";
		LOGR(cn, (char *)dbgstr.c_str());
		return 1;
		}

	// WebSocket endpoint
	// Test with the following JavaScript:
	//   var ws=new WebSocket("ws://localhost:9001/gemalto");
	//   ws.onmessage=function(evt){console.log(evt.data);};
	//   ws.send("test");
	auto &gemaltoEndpoint = wsServer->endpoint["^/gemalto/?$"];

	LOGR(cn, "iSignWSServcer running - ws://localhost:9022/gemalto");

	gemaltoEndpoint.on_message = [](shared_ptr<WsServer::Connection> connection, shared_ptr<WsServer::Message> message)
		{
		auto message_str = message->string();

		std::string dbgstr = "Server: Message received: " + message_str;
		LOGR(cn, (char *)dbgstr.c_str());

		// decode json
		Json::Value root;   // will contains the root value after parsing.
		Json::Reader reader;
		std::string functionName;
		std::string pin;
		GemaltoPKCS11 gemaltoPKCS11;
		std::string certsJoson;
		std::string selectedCert;
		std::string digestToSign;
		std::string hashAlgo;

		//JSON Format: "{\"function\":\"getAllUserCertificates\", \"Pin\":\"Bastian23\", \"selectedCert\":\"111\", \"digestToSign\":\"222\", \"hashAlgo\": \"SHA256\"}";
		//
		bool parsingSuccessful = reader.parse(message_str, root);
		if (parsingSuccessful)
			{
			pin = root.get("pin", "").asString();
			selectedCert = root.get("selectedCert", "").asString();
			functionName = root["function"].asString();
			digestToSign = root["digestToSign"].asString();
			hashAlgo = root["hashAlgo"].asString();


			if (functionName == "getAllUserCertificates")
				{
				LOGR(cn, "calling: emaltoPKCS11.getAllUserCertificates(pin)");
				// TODO: error handling
				certsJoson = gemaltoPKCS11.getAllUserCertificates(pin);

				dbgstr = "Server getAllUserCertificates: returning - " + certsJoson;
				LOGR(cn, (char *)dbgstr.c_str());

				auto send_stream = make_shared<WsServer::SendStream>();
				*send_stream << certsJoson;

				// connection->send is an asynchronous function
				connection->send(send_stream, [](const SimpleWeb::error_code &ec)
					{
					if (ec) {
						// See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
						std::string dbgstr = "Server: Error sending message. Error message: " + ec.message();
						LOGR_ERR(cn, (char *)dbgstr.c_str());
						}
					});
				}

			if (functionName == "getCertificatesOfUser")
				{
				LOGR(cn, "calling: gemaltoPKCS11.getCertificatesOfUser(pin, selectedCert)");
				// TODO: error handling
				certsJoson = gemaltoPKCS11.getCertificatesOfUser(pin, selectedCert);

				dbgstr = "Server getCertificatesOfUser: returning - " + certsJoson;
				LOGR(cn, (char *)dbgstr.c_str());

				auto send_stream = make_shared<WsServer::SendStream>();
				*send_stream << certsJoson;

				// connection->send is an asynchronous function
				connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
					if (ec) {
						std::string dbgstr = "Server: Error sending message. Error message: " + ec.message();
						LOGR_ERR(cn, (char *)dbgstr.c_str());
						}
					});
				}

			if (functionName == "sign")
				{
				LOGR(cn, "calling: gemaltoPKCS11.sign(pin, digestToSign, selectedCert, hashAlgo)");

				// TODO: error handling
				std::string signedDigest = gemaltoPKCS11.sign(pin, digestToSign, selectedCert, hashAlgo);

				dbgstr = "Server sign: returning - " + signedDigest;
				LOGR_ERR(cn, (char *)dbgstr.c_str());

				auto send_stream = make_shared<WsServer::SendStream>();
				*send_stream << signedDigest;

				// connection->send is an asynchronous function
				connection->send(send_stream, [](const SimpleWeb::error_code &ec) {
					if (ec) {
						std::string dbgstr = "Server: Error sending message. Error message: " + ec.message();
						LOGR_ERR(cn, (char *)dbgstr.c_str());
						}
					});
				}

			if (functionName == "getVersion")
				{
				return"{\"version\"=\"1.0.0\"}";
				}
			}
		};

	gemaltoEndpoint.on_open = [](shared_ptr<WsServer::Connection> connection) {
		std::string dbgstr = "Server: Opened connection";
		LOGR(cn, (char *)dbgstr.c_str());
		};

	// See RFC 6455 7.4.1. for status codes
	gemaltoEndpoint.on_close = [](shared_ptr<WsServer::Connection> connection, int status, const string & /*reason*/) {
		std::string dbgstr = "Server: Closed connection with status code " + status;
		LOGR(cn, (char *)dbgstr.c_str());
		};

	// See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
	gemaltoEndpoint.on_error = [](shared_ptr<WsServer::Connection> connection, const SimpleWeb::error_code &ec) {
		std::string dbgstr = "Server: Error in connection Error message: " + ec.message();
		LOGR(cn, (char *)dbgstr.c_str());
		};

	// start the server thread
	thread server_thread([&wsServer]() {
		// Start WS-server
		wsServer->start();
		});

	// wait until server thred finishes
	server_thread.join();

	// delete server we created with new
	delete wsServer;

	return 0;
	}

